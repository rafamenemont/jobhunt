import { Component } from "react";
import Logo from "../../assets/logo.svg";
import "./styles.css";

class Header extends Component {
  render() {
    return (
      <header className="header-container">
        <img src={Logo}></img>
      </header>
    );
  }
}

export default Header;
