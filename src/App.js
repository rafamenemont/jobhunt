import { Component } from "react";
import "./styles/App.css";
import Card from "./components/Card";
import Header from "./components/Header";
import axios from "axios";

class App extends Component {
  state = {
    jobs: [],
    loading: true,
  };

  componentDidMount() {
    this.state.jobs.length === 0 &&
      fetch(`https://jobhunt-api.herokuapp.com/jobs`)
        .then((response) => response.json())
        .then((response) => this.setState({ jobs: response, loading: false }));
  }


  javaJobs = () => {
    fetch("https://jobhunt-api.herokuapp.com/jobs?description=javascript")
      .then((response) => response.json())
      .then((response) => this.setState({ jobs: response, loading: false }));
  }

  pythonJobs = () => {
    fetch("https://jobhunt-api.herokuapp.com/jobs?description=python")
      .then((response) => response.json())
      .then((response) => this.setState({ jobs: response, loading: false }));
  }

  render() {
    const { jobs, loading } = this.state;
    return (
      <div className="App">
        <Header />
        <h3>🔥Trending Jobs</h3>
        <button onClick={this.pythonJobs}>Python Jobs</button>
        <button onClick={this.javaJobs}>Javascript Jobs</button>

        <div className="cardList">
          {loading && <div class="spinner"></div>}
          {jobs.map((job) => (
            <Card job={job} key={job.id} />
          ))}
        </div>
      </div>
    );
  }
}

export default App;
